Description: Patch to compile against BouncyCastle 1.48
Author: Emmanuel Bourg <ebourg@apache.org>
Forwarded: yes
Bug: https://github.com/italiangrid/voms-api-java/pull/4
Bug-Debian: http://bugs.debian.org/713204
--- a/pom.xml
+++ b/pom.xml
@@ -17,7 +17,7 @@
 		<assembly.java.dir>/usr/share/java</assembly.java.dir>
 		<assembly.doc.dir>/usr/share/doc/${project.name}-${project.version}</assembly.doc.dir>
 		<assembly.javadoc.dir>/usr/share/javadoc/${project.name}</assembly.javadoc.dir>
-		<bouncycastle.version>1.45</bouncycastle.version>
+		<bouncycastle.version>1.48</bouncycastle.version>
 	</properties>
 
 	<developers>
@@ -95,7 +95,7 @@
 
 		<dependency>
 			<groupId>org.bouncycastle</groupId>
-			<artifactId>bcprov-ext-jdk16</artifactId>
+			<artifactId>bcpkix-jdk15on</artifactId>
 			<version>${bouncycastle.version}</version>
 		</dependency>
 
--- a/src/main/java/org/glite/voms/PKIUtils.java
+++ b/src/main/java/org/glite/voms/PKIUtils.java
@@ -63,7 +63,7 @@
 import org.bouncycastle.asn1.ASN1TaggedObject;
 import org.bouncycastle.asn1.DERBitString;
 import org.bouncycastle.asn1.DERInteger;
-import org.bouncycastle.asn1.DERObject;
+import org.bouncycastle.asn1.ASN1Primitive;
 import org.bouncycastle.asn1.DERObjectIdentifier;
 import org.bouncycastle.asn1.DEROctetString;
 import org.bouncycastle.asn1.DERTaggedObject;
@@ -334,14 +334,14 @@
     }
 
     static private BigInteger getAuthorityCertificateSerialNumber(AuthorityKeyIdentifier akid) {
-        DERObject obj = akid.getDERObject();
+        ASN1Primitive obj = akid.toASN1Primitive();
         ASN1Sequence seq = ASN1Sequence.getInstance(obj);
 
         for (int i = 0; i < seq.size(); i++) {
-            DERObject o = (DERObject) seq.getObjectAt(i);
+            ASN1Primitive o = (ASN1Primitive) seq.getObjectAt(i);
             if ((o instanceof ASN1TaggedObject) &&
                 (((ASN1TaggedObject)o).getTagNo() == 2)) {
-                DERObject realObject = ((ASN1TaggedObject)o).getObject();
+                ASN1Primitive realObject = ((ASN1TaggedObject)o).getObject();
                 if (realObject instanceof DERInteger) {
                     return ((DERInteger)realObject).getValue();
                 }
@@ -351,11 +351,11 @@
     }
 
     static private GeneralNames getAuthorityCertIssuer(AuthorityKeyIdentifier akid) {
-        DERObject obj = akid.getDERObject();
+        ASN1Primitive obj = akid.toASN1Primitive();
         ASN1Sequence seq = ASN1Sequence.getInstance(obj);
 
         for (int i = 0; i < seq.size(); i++) {
-            DERObject o = (DERObject) seq.getObjectAt(i);
+            ASN1Primitive o = (ASN1Primitive) seq.getObjectAt(i);
             if ((o instanceof ASN1TaggedObject) &&
                 (((ASN1TaggedObject)o).getTagNo() == 1)) {
                 return GeneralNames.getInstance(((DERTaggedObject)o), false);
@@ -365,7 +365,7 @@
     }
 
     static private GeneralName[] getNames(GeneralNames gns) {
-        DERObject obj = gns.getDERObject();
+        ASN1Primitive obj = gns.toASN1Primitive();
         ArrayList v = new ArrayList();
 
         ASN1Sequence seq = (ASN1Sequence)obj;
@@ -515,7 +515,7 @@
                 }
                 logger.debug(str.toString());
         
-                DERObject dobj = null;
+                ASN1Primitive dobj = null;
                 try {
                     dobj = new ASN1InputStream(new ByteArrayInputStream(keybytes)).readObject();
                     logger.debug("Class = " + dobj.getClass());
@@ -608,7 +608,7 @@
                 ASN1OctetString string = new DEROctetString(akid);
                 byte[] llist2 = string.getOctets();
                         
-                DERObject dobj = null;
+                ASN1Primitive dobj = null;
                 try {
                     dobj = new ASN1InputStream(new ByteArrayInputStream(llist2)).readObject();
                     dobj = new ASN1InputStream(new ByteArrayInputStream(((DEROctetString)dobj).getOctets())).readObject();
@@ -620,7 +620,7 @@
                     throw new IllegalArgumentException("While extracting Authority Key Identifier " + e.getMessage(), e);
                 }
                 
-                return new AuthorityKeyIdentifier(ASN1Sequence.getInstance(dobj));
+                return AuthorityKeyIdentifier.getInstance(ASN1Sequence.getInstance(dobj));
             }
         }
         return null;
@@ -637,7 +637,7 @@
         if (cert != null) {
             byte[] akid = cert.getExtensionValue(SUBJECT_KEY_IDENTIFIER);
             if (akid != null) {
-                DERObject dobj = null;
+                ASN1Primitive dobj = null;
                 try {
                     dobj = new ASN1InputStream(new ByteArrayInputStream(akid)).readObject();
                     dobj = new ASN1InputStream(new ByteArrayInputStream(((DEROctetString)dobj).getOctets())).readObject();
@@ -662,7 +662,7 @@
         if (cert != null) {
             byte[] akid = cert.getExtensionValue(BASIC_CONSTRAINTS_IDENTIFIER);
             if (akid != null) {
-                DERObject dobj = null;
+                ASN1Primitive dobj = null;
                 try {
                     dobj = new ASN1InputStream(new ByteArrayInputStream(akid)).readObject();
                 }
@@ -670,7 +670,7 @@
                     throw new IllegalArgumentException("While extracting Subject Key Identifier " + e.getMessage());
                 }
 
-                return new BasicConstraints(ASN1Sequence.getInstance(dobj));
+                return BasicConstraints.getInstance(ASN1Sequence.getInstance(dobj));
             }
         }
         return null;
--- a/src/main/java/org/glite/voms/PKIVerifier.java
+++ b/src/main/java/org/glite/voms/PKIVerifier.java
@@ -58,7 +58,7 @@
 
 import org.apache.log4j.Logger;
 import org.bouncycastle.asn1.ASN1InputStream;
-import org.bouncycastle.asn1.DERObject;
+import org.bouncycastle.asn1.ASN1Primitive;
 import org.bouncycastle.asn1.DERObjectIdentifier;
 import org.bouncycastle.asn1.DEROctetString;
 import org.bouncycastle.asn1.x509.X509Extension;
@@ -468,7 +468,7 @@
         else
             ext = new X509Extension(true, new DEROctetString(payload));
 
-        DERObject obj = null;
+        ASN1Primitive obj = null;
 
         try {
             obj = new ASN1InputStream(new ByteArrayInputStream(ext.getValue().getOctets())).readObject();
--- a/src/main/java/org/glite/voms/VOMSAttribute.java
+++ b/src/main/java/org/glite/voms/VOMSAttribute.java
@@ -218,7 +218,7 @@
 
         GeneralNames names = myAC.getHolder().getIssuer();
 
-        Enumeration e = ((ASN1Sequence) names.getDERObject()).getObjects();
+        Enumeration e = ((ASN1Sequence) names.toASN1Primitive()).getObjects();
         if (e.hasMoreElements()) {
             GeneralName gn = (GeneralName)e.nextElement();
             
--- a/src/main/java/org/glite/voms/ac/ACCerts.java
+++ b/src/main/java/org/glite/voms/ac/ACCerts.java
@@ -37,9 +37,9 @@
 
 import org.bouncycastle.jce.provider.BouncyCastleProvider;
 import org.bouncycastle.asn1.ASN1Sequence;
-import org.bouncycastle.asn1.DEREncodable;
+import org.bouncycastle.asn1.ASN1Encodable;
 import org.bouncycastle.asn1.ASN1EncodableVector;
-import org.bouncycastle.asn1.DERObject;
+import org.bouncycastle.asn1.ASN1Primitive;
 import org.bouncycastle.asn1.DERSequence;
 import org.bouncycastle.asn1.x509.X509CertificateStructure;
 import org.bouncycastle.jce.provider.X509CertificateObject;
@@ -49,7 +49,7 @@
  *
  * @author Vincenzo Ciaschini.
  */
-public class ACCerts implements DEREncodable {
+public class ACCerts implements ASN1Encodable {
     List l;
 
     /**
@@ -97,7 +97,7 @@
                 ASN1Sequence s = ASN1Sequence.getInstance(o);
                 byte[] data = null;
                 try {
-                      data = new X509CertificateObject(X509CertificateStructure.getInstance(s)).getEncoded();
+                      data = new X509CertificateObject(org.bouncycastle.asn1.x509.Certificate.getInstance(s)).getEncoded();
                       l.add((X509Certificate)cf.generateCertificate(new ByteArrayInputStream(data)));
                 }
                 catch(Exception ex) {
@@ -141,7 +141,7 @@
      *
      * @return the DERObject
      */
-    public DERObject getDERObject() {
+    public ASN1Primitive toASN1Primitive() {
         ASN1EncodableVector v = new ASN1EncodableVector();
 
         ListIterator li = l.listIterator();
--- a/src/main/java/org/glite/voms/ac/ACTarget.java
+++ b/src/main/java/org/glite/voms/ac/ACTarget.java
@@ -26,10 +26,10 @@
 
 import org.bouncycastle.asn1.ASN1Sequence;
 import org.bouncycastle.asn1.ASN1TaggedObject;
-import org.bouncycastle.asn1.DEREncodable;
+import org.bouncycastle.asn1.ASN1Encodable;
 import org.bouncycastle.asn1.ASN1EncodableVector;
 import org.bouncycastle.asn1.DERIA5String;
-import org.bouncycastle.asn1.DERObject;
+import org.bouncycastle.asn1.ASN1Primitive;
 import org.bouncycastle.asn1.DERSequence;
 import org.bouncycastle.asn1.DERTaggedObject;
 import org.bouncycastle.asn1.x509.GeneralName;
@@ -62,7 +62,7 @@
  *
  * @author Vincenzo Ciaschini
  */
-public class ACTarget implements DEREncodable {
+public class ACTarget implements ASN1Encodable {
     private GeneralName  name;
     private GeneralName  group;
     private IssuerSerial cert;
@@ -127,7 +127,7 @@
      * @return the IssuerSerial as String.
      */
     public String getIssuerSerialString() {
-        ASN1Sequence seq = ASN1Sequence.getInstance(cert.getIssuer().getDERObject());
+        ASN1Sequence seq = ASN1Sequence.getInstance(cert.getIssuer().toASN1Primitive());
         GeneralName  name  = GeneralName.getInstance(seq.getObjectAt(0));
 
         return NameConverter.getInstance(name).getAsString() + ":" + 
@@ -149,7 +149,7 @@
      * @param s the name.
      */
     public void setName(String s) {
-        name = new GeneralName(new DERIA5String(s), 6);
+        name = new GeneralName(6, new DERIA5String(s));
     }
 
     /**
@@ -167,7 +167,7 @@
      * @param s the group name.
      */
     public void setGroup(String s) {
-        group = new GeneralName(new DERIA5String(s), 6);
+        group = new GeneralName(6, new DERIA5String(s));
     }
 
     /**
@@ -189,13 +189,13 @@
         int ch = s.lastIndexOf(':');
         if (ch != -1) {
             String iss = s.substring(0, ch);
-            GeneralName nm = new GeneralName(new DERIA5String(iss), 6);
-            ASN1Sequence seq = ASN1Sequence.getInstance(name.getDERObject());
+            GeneralName nm = new GeneralName(6, new DERIA5String(iss));
+            ASN1Sequence seq = ASN1Sequence.getInstance(name.toASN1Primitive());
 
             ASN1EncodableVector v = new ASN1EncodableVector();
             v.add(nm);
             v.add(seq);
-            cert = new IssuerSerial(new DERSequence(v));
+            cert = IssuerSerial.getInstance(new DERSequence(v));
         }
         else throw new IllegalArgumentException("cannot identify issuer and serial");
     }
@@ -238,7 +238,7 @@
                 case 2:
                     group = null;
                     name = null;
-                    cert = new IssuerSerial((ASN1Sequence)obj.getObject());
+                    cert = IssuerSerial.getInstance((ASN1Sequence)obj.getObject());
                     break;
                 default:
                     throw new IllegalArgumentException("Bad tag in encoding ACTarget");
@@ -255,7 +255,7 @@
      *
      * @return the DERObject
      */
-    public DERObject getDERObject() {
+    public ASN1Primitive toASN1Primitive() {
         ASN1EncodableVector v = new ASN1EncodableVector();
 
         if (name != null)
--- a/src/main/java/org/glite/voms/ac/ACTargets.java
+++ b/src/main/java/org/glite/voms/ac/ACTargets.java
@@ -30,9 +30,9 @@
 import java.util.Vector;
 
 import org.bouncycastle.asn1.ASN1Sequence;
-import org.bouncycastle.asn1.DEREncodable;
+import org.bouncycastle.asn1.ASN1Encodable;
 import org.bouncycastle.asn1.ASN1EncodableVector;
-import org.bouncycastle.asn1.DERObject;
+import org.bouncycastle.asn1.ASN1Primitive;
 import org.bouncycastle.asn1.DERSequence;
 
 /**
@@ -41,7 +41,7 @@
  *
  * @author Vincenzo Ciaschini
  */
-public class ACTargets implements DEREncodable {
+public class ACTargets implements ASN1Encodable {
     private List l;
     private List parsed;
 
@@ -116,7 +116,7 @@
      *
      * @return the DERObject
      */
-    public DERObject getDERObject() {
+    public ASN1Primitive toASN1Primitive() {
         ASN1EncodableVector v = new ASN1EncodableVector();
 
         ListIterator li = l.listIterator();
--- a/src/main/java/org/glite/voms/ac/AttCertIssuer.java
+++ b/src/main/java/org/glite/voms/ac/AttCertIssuer.java
@@ -35,9 +35,10 @@
 
 import org.bouncycastle.asn1.ASN1Sequence;
 import org.bouncycastle.asn1.ASN1TaggedObject;
-import org.bouncycastle.asn1.DEREncodable;
-import org.bouncycastle.asn1.DERObject;
+import org.bouncycastle.asn1.ASN1Encodable;
+import org.bouncycastle.asn1.ASN1Primitive;
 import org.bouncycastle.asn1.DERTaggedObject;
+import org.bouncycastle.asn1.x509.GeneralName;
 import org.bouncycastle.asn1.x509.GeneralNames;
 
 
@@ -47,12 +48,12 @@
  *
  * @author Joni Hahkala, Olle Mulmo
  */
-public class AttCertIssuer implements DEREncodable {
+public class AttCertIssuer implements ASN1Encodable {
     GeneralNames v1Form;
     V2Form v2Form;
     int version = -1;
 
-    public AttCertIssuer(DEREncodable obj) {
+    public AttCertIssuer(ASN1Encodable obj) {
         if (obj instanceof ASN1TaggedObject) {
             ASN1TaggedObject cObj = (ASN1TaggedObject) obj;
 
@@ -62,7 +63,7 @@
                 version = 2;
             }
         } else if (obj instanceof ASN1Sequence) {
-            v1Form = new GeneralNames((ASN1Sequence) obj);
+            v1Form = new GeneralNames(GeneralName.getInstance(obj));
             version = 1;
         }
 
@@ -107,10 +108,10 @@
      *
      * </pre>
      */
-    public DERObject getDERObject() {
+    public ASN1Primitive toASN1Primitive() {
         switch (version) {
         case 1:
-            return v1Form.getDERObject();
+            return v1Form.toASN1Primitive();
 
         case 2:
             return new DERTaggedObject(true, 0, v2Form);
--- a/src/main/java/org/glite/voms/ac/AttributeCertificate.java
+++ b/src/main/java/org/glite/voms/ac/AttributeCertificate.java
@@ -58,10 +58,10 @@
 import org.bouncycastle.asn1.ASN1InputStream;
 import org.bouncycastle.asn1.DERBitString;
 import org.bouncycastle.asn1.DERSet;
-import org.bouncycastle.asn1.DEREncodable;
+import org.bouncycastle.asn1.ASN1Encodable;
 import org.bouncycastle.asn1.DERGeneralizedTime;
 import org.bouncycastle.asn1.DERInteger;
-import org.bouncycastle.asn1.DERObject;
+import org.bouncycastle.asn1.ASN1Primitive;
 import org.bouncycastle.asn1.DERObjectIdentifier;
 import org.bouncycastle.asn1.DEROutputStream;
 import org.bouncycastle.asn1.DERSequence;
@@ -81,15 +81,15 @@
  *
  * @author Joni Hahkala, Olle Mulmo
  */
-public class AttributeCertificate implements DEREncodable {
+public class AttributeCertificate implements ASN1Encodable {
     protected static final Logger logger = Logger.getLogger(AttributeCertificate.class);
     AttributeCertificateInfo acInfo;
     AlgorithmIdentifier signatureAlgorithm;
     DERBitString signatureValue;
-    DERObject signedObj = null;
+    ASN1Primitive signedObj = null;
 
     public AttributeCertificate(ASN1Sequence seq) throws IOException {
-        signedObj = ((ASN1Sequence)seq.getObjectAt(0)).getDERObject();
+        signedObj = ((ASN1Sequence)seq.getObjectAt(0)).toASN1Primitive();
         acInfo = new AttributeCertificateInfo((ASN1Sequence) seq.getObjectAt(0));
         signatureAlgorithm = AlgorithmIdentifier.getInstance(seq.getObjectAt(1));
         signatureValue = (DERBitString) seq.getObjectAt(2);
@@ -254,7 +254,7 @@
             return null;
         }
 
-        ASN1Sequence seq = (ASN1Sequence) acInfo.getIssuer().getIssuerName().getDERObject();
+        ASN1Sequence seq = (ASN1Sequence) acInfo.getIssuer().getIssuerName().toASN1Primitive();
 
         for (Enumeration e = seq.getObjects(); e.hasMoreElements();) {
             GeneralName gn = GeneralName.getInstance(e.nextElement());
@@ -276,7 +276,7 @@
             return null;
         }
 
-        ASN1Sequence seq = (ASN1Sequence) acInfo.getIssuer().getIssuerName().getDERObject();
+        ASN1Sequence seq = (ASN1Sequence) acInfo.getIssuer().getIssuerName().toASN1Primitive();
         for (Enumeration e = seq.getObjects(); e.hasMoreElements();) {
             Object o = e.nextElement();
             GeneralName gn = GeneralName.getInstance( o);
@@ -300,7 +300,7 @@
 
         GeneralNames gns = acInfo.getHolder().getIssuer();
 
-        for (Enumeration e = ((ASN1Sequence)gns.getDERObject()).getObjects(); e.hasMoreElements();) {
+        for (Enumeration e = ((ASN1Sequence)gns.toASN1Primitive()).getObjects(); e.hasMoreElements();) {
             GeneralName gn = (GeneralName)e.nextElement();
 
             if (gn.getTagNo() == 4) {
@@ -458,7 +458,7 @@
      *  }
      * </pre>
      */
-    public DERObject getDERObject() {
+    public ASN1Primitive toASN1Primitive() {
         ASN1EncodableVector v = new ASN1EncodableVector();
 
         v.add(acInfo);
--- a/src/main/java/org/glite/voms/ac/AttributeCertificateInfo.java
+++ b/src/main/java/org/glite/voms/ac/AttributeCertificateInfo.java
@@ -46,12 +46,12 @@
 import org.bouncycastle.asn1.ASN1TaggedObject;
 import org.bouncycastle.asn1.DERBitString;
 import org.bouncycastle.asn1.DERSet;
-import org.bouncycastle.asn1.DEREncodable;
+import org.bouncycastle.asn1.ASN1Encodable;
 import org.bouncycastle.asn1.ASN1EncodableVector;
 import org.bouncycastle.asn1.DERGeneralizedTime;
 import org.bouncycastle.asn1.DERIA5String;
 import org.bouncycastle.asn1.DERInteger;
-import org.bouncycastle.asn1.DERObject;
+import org.bouncycastle.asn1.ASN1Primitive;
 import org.bouncycastle.asn1.DERObjectIdentifier;
 import org.bouncycastle.asn1.DEROctetString;
 import org.bouncycastle.asn1.DERSequence;
@@ -68,7 +68,7 @@
  *
  * @author Joni Hahkala, Olle Mulmo
  */
-public class AttributeCertificateInfo implements DEREncodable {
+public class AttributeCertificateInfo implements ASN1Encodable {
     DERInteger version;
     Holder holder;
     AttCertIssuer issuer;
@@ -122,7 +122,7 @@
             s3 = (ASN1Sequence) new DERSequence(v);
         }
 
-        attrCertValidityPeriod = new AttCertValidityPeriod(s3);
+        attrCertValidityPeriod = AttCertValidityPeriod.getInstance(s3);
         attributes = (ASN1Sequence) seq.getObjectAt(6);
 
         // getting FQANs
@@ -138,7 +138,7 @@
                     for (Enumeration s = set.getObjects(); s.hasMoreElements();) {
                         IetfAttrSyntax attr = new IetfAttrSyntax((ASN1Sequence)s.nextElement());
                         String url = ((DERIA5String) GeneralName.getInstance(((ASN1Sequence) attr.getPolicyAuthority()
-                                                                              .getDERObject()).getObjectAt(0))
+                                                                              .toASN1Primitive()).getObjectAt(0))
                                       .getName()).getString();
                         int idx = url.indexOf("://");
 
@@ -181,7 +181,7 @@
         // check if the following two can be detected better!!! 
         // for example, is it possible to have only the extensions? how to detect this?
         if (seq.size() > 8) {
-            issuerUniqueID = new DERBitString(seq.getObjectAt(7));
+            issuerUniqueID = DERBitString.getInstance(seq.getObjectAt(7));
             extensions = new X509Extensions((ASN1Sequence) seq.getObjectAt(8));
         } else if (seq.size() > 7) {
             extensions = new X509Extensions((ASN1Sequence) seq.getObjectAt(7));
@@ -190,7 +190,7 @@
         // start parsing of known extensions
         if (extensions.getExtension(AC_TARGET_OID_DER) != null) {
             byte[] data = (extensions.getExtension(AC_TARGET_OID_DER).getValue().getOctets());
-            DERObject dobj = null;
+            ASN1Primitive dobj = null;
             try {
                 dobj = new ASN1InputStream(new ByteArrayInputStream(data)).readObject();
                 acTargets = new ACTargets(ASN1Sequence.getInstance(dobj));
@@ -201,7 +201,7 @@
 
         if (extensions.getExtension(AC_CERTS_OID_DER) != null) {
             byte[] data = (extensions.getExtension(AC_CERTS_OID_DER).getValue().getOctets());
-            DERObject dobj = null;
+            ASN1Primitive dobj = null;
             try {
                 dobj = new ASN1InputStream(new ByteArrayInputStream(data)).readObject();
                 acCerts = new ACCerts(ASN1Sequence.getInstance(dobj));
@@ -212,7 +212,7 @@
 
         if (extensions.getExtension(AC_FULL_ATTRIBUTES_OID_DER) != null) {
             byte[] data = (extensions.getExtension(AC_FULL_ATTRIBUTES_OID_DER).getValue().getOctets());
-            DERObject dobj = null;
+            ASN1Primitive dobj = null;
             try {
                 dobj = new ASN1InputStream(new ByteArrayInputStream(data)).readObject();
 
@@ -335,7 +335,7 @@
      *
      * </pre>
      */
-    public DERObject getDERObject() {
+    public ASN1Primitive toASN1Primitive() {
         ASN1EncodableVector v = new ASN1EncodableVector();
         v.add(version);
         v.add(holder);
--- a/src/main/java/org/glite/voms/ac/AttributeHolder.java
+++ b/src/main/java/org/glite/voms/ac/AttributeHolder.java
@@ -31,10 +31,10 @@
 import java.util.Vector;
 
 import org.bouncycastle.asn1.ASN1Sequence;
-import org.bouncycastle.asn1.DEREncodable;
+import org.bouncycastle.asn1.ASN1Encodable;
 import org.bouncycastle.asn1.ASN1EncodableVector;
 import org.bouncycastle.asn1.DERIA5String;
-import org.bouncycastle.asn1.DERObject;
+import org.bouncycastle.asn1.ASN1Primitive;
 import org.bouncycastle.asn1.DERSequence;
 import org.bouncycastle.asn1.x509.GeneralName;
 import org.bouncycastle.asn1.x509.GeneralNames;
@@ -45,7 +45,7 @@
  *
  * @author Vincenzo Ciaschini
  */
-public class AttributeHolder implements DEREncodable {
+public class AttributeHolder implements ASN1Encodable {
     private List l;
     private GeneralNames grantor;
 
@@ -98,7 +98,7 @@
      * @return the grantor.
      */
     public String getGrantor() {
-        ASN1Sequence seq = ASN1Sequence.getInstance(grantor.getDERObject());
+        ASN1Sequence seq = ASN1Sequence.getInstance(grantor.toASN1Primitive());
         GeneralName  name  = GeneralName.getInstance(seq.getObjectAt(0));
         return DERIA5String.getInstance(name.getName()).getString();
     }
@@ -118,7 +118,7 @@
      *
      * @return the DERObject
      */
-    public DERObject getDERObject() {
+    public ASN1Primitive toASN1Primitive() {
         ASN1EncodableVector v = new ASN1EncodableVector();
 
         v.add(grantor);
--- a/src/main/java/org/glite/voms/ac/FullAttributes.java
+++ b/src/main/java/org/glite/voms/ac/FullAttributes.java
@@ -30,9 +30,9 @@
 import java.util.Vector;
 
 import org.bouncycastle.asn1.ASN1Sequence;
-import org.bouncycastle.asn1.DEREncodable;
+import org.bouncycastle.asn1.ASN1Encodable;
 import org.bouncycastle.asn1.ASN1EncodableVector;
-import org.bouncycastle.asn1.DERObject;
+import org.bouncycastle.asn1.ASN1Primitive;
 import org.bouncycastle.asn1.DERSequence;
 
 /**
@@ -41,7 +41,7 @@
  *
  * @author Vincenzo Ciaschini
  */
-public class FullAttributes implements DEREncodable {
+public class FullAttributes implements ASN1Encodable {
     private List l;
 
     /**
@@ -93,7 +93,7 @@
      *
      * @return the DERObject
      */
-    public DERObject getDERObject() {
+    public ASN1Primitive toASN1Primitive() {
         ASN1EncodableVector v2 = new ASN1EncodableVector();
 
         for (ListIterator li = l.listIterator(); li.hasNext(); ) {
--- a/src/main/java/org/glite/voms/ac/GenericAttribute.java
+++ b/src/main/java/org/glite/voms/ac/GenericAttribute.java
@@ -26,9 +26,9 @@
 
 import org.bouncycastle.asn1.ASN1OctetString;
 import org.bouncycastle.asn1.ASN1Sequence;
-import org.bouncycastle.asn1.DEREncodable;
+import org.bouncycastle.asn1.ASN1Encodable;
 import org.bouncycastle.asn1.ASN1EncodableVector;
-import org.bouncycastle.asn1.DERObject;
+import org.bouncycastle.asn1.ASN1Primitive;
 import org.bouncycastle.asn1.DEROctetString;
 import org.bouncycastle.asn1.DERSequence;
 
@@ -38,7 +38,7 @@
  *
  * @author Vincenzo Ciaschini
  */
-public class GenericAttribute implements DEREncodable {
+public class GenericAttribute implements ASN1Encodable {
     private String name;
     private String value;
     private String qualifier;
@@ -115,7 +115,7 @@
      *
      * @return the DERObject
      */
-    public DERObject getDERObject() {
+    public ASN1Primitive toASN1Primitive() {
         ASN1EncodableVector v = new ASN1EncodableVector();
 
         v.add(new DEROctetString(name.getBytes()));
--- a/src/main/java/org/glite/voms/ac/Holder.java
+++ b/src/main/java/org/glite/voms/ac/Holder.java
@@ -40,10 +40,10 @@
 import org.bouncycastle.asn1.ASN1EncodableVector;
 import org.bouncycastle.asn1.ASN1Sequence;
 import org.bouncycastle.asn1.ASN1TaggedObject;
-import org.bouncycastle.asn1.DEREncodable;
+import org.bouncycastle.asn1.ASN1Encodable;
 import org.bouncycastle.asn1.ASN1EncodableVector;
 import org.bouncycastle.asn1.DERInteger;
-import org.bouncycastle.asn1.DERObject;
+import org.bouncycastle.asn1.ASN1Primitive;
 import org.bouncycastle.asn1.DEROutputStream;
 import org.bouncycastle.asn1.DERSequence;
 import org.bouncycastle.asn1.DERTaggedObject;
@@ -66,7 +66,7 @@
  *  }
  * </pre>
  */
-public class Holder implements DEREncodable {
+public class Holder implements ASN1Encodable {
     IssuerSerial baseCertificateID = null;
     GeneralNames entityName = null;
     ObjectDigestInfo objectDigestInfo = null;
@@ -79,12 +79,12 @@
         ASN1EncodableVector v = new ASN1EncodableVector();
         v.add(Util.x500nameToGeneralNames(issuer));
         v.add(new DERInteger(serial));
-        baseCertificateID = new IssuerSerial(new DERSequence(v));
+        baseCertificateID = IssuerSerial.getInstance(new DERSequence(v));
     }
 
     public Holder(ASN1Sequence seq) {
         for (Enumeration e = seq.getObjects(); e.hasMoreElements();) {
-            DERObject obj = (DERObject) e.nextElement();
+            ASN1Primitive obj = (ASN1Primitive) e.nextElement();
 
             if (!(obj instanceof ASN1TaggedObject)) {
                 throw new IllegalArgumentException("Holder element not tagged");
@@ -94,7 +94,7 @@
 
             switch (tObj.getTagNo()) {
             case 0:
-                baseCertificateID = new IssuerSerial((ASN1Sequence) tObj.getObject());
+                baseCertificateID = IssuerSerial.getInstance((ASN1Sequence) tObj.getObject());
 
                 break;
 
@@ -123,7 +123,7 @@
     }
 
     protected static boolean matchesDN(X500Principal subject, GeneralNames targets) {
-        Enumeration e = ((ASN1Sequence) targets.getDERObject()).getObjects();
+        Enumeration e = ((ASN1Sequence) targets.toASN1Primitive()).getObjects();
 
         while (e.hasMoreElements()) {
             GeneralName gn = GeneralName.getInstance(e.nextElement());
@@ -175,7 +175,7 @@
         return false;
     }
 
-    public DERObject getDERObject() {
+    public ASN1Primitive toASN1Primitive() {
         ASN1EncodableVector v = new ASN1EncodableVector();
 
         if (baseCertificateID != null) {
--- a/src/main/java/org/glite/voms/ac/IetfAttrSyntax.java
+++ b/src/main/java/org/glite/voms/ac/IetfAttrSyntax.java
@@ -37,9 +37,9 @@
 
 import org.bouncycastle.asn1.ASN1Sequence;
 import org.bouncycastle.asn1.ASN1TaggedObject;
-import org.bouncycastle.asn1.DEREncodable;
+import org.bouncycastle.asn1.ASN1Encodable;
 import org.bouncycastle.asn1.ASN1EncodableVector;
-import org.bouncycastle.asn1.DERObject;
+import org.bouncycastle.asn1.ASN1Primitive;
 import org.bouncycastle.asn1.DERObjectIdentifier;
 import org.bouncycastle.asn1.DEROctetString;
 import org.bouncycastle.asn1.DERSequence;
@@ -65,7 +65,7 @@
  *
  * @author mulmo
  */
-public class IetfAttrSyntax implements DEREncodable {
+public class IetfAttrSyntax implements ASN1Encodable {
     public static final int VALUE_OCTETS = 1;
     public static final int VALUE_OID = 2;
     public static final int VALUE_UTF8 = 3;
@@ -92,7 +92,7 @@
         seq = (ASN1Sequence) seq.getObjectAt(i);
 
         for (Enumeration e = seq.getObjects(); e.hasMoreElements();) {
-            DERObject obj = (DERObject) e.nextElement();
+            ASN1Primitive obj = (ASN1Primitive) e.nextElement();
             int type;
 
             if (obj instanceof DERObjectIdentifier) {
@@ -129,7 +129,7 @@
         return values;
     }
 
-    public DERObject getDERObject() {
+    public ASN1Primitive toASN1Primitive() {
         ASN1EncodableVector v = new ASN1EncodableVector();
 
         if (policyAuthority != null) {
@@ -139,7 +139,7 @@
         ASN1EncodableVector v2 = new ASN1EncodableVector();
 
         for (Iterator i = values.iterator(); i.hasNext();) {
-            v2.add((DEREncodable) i.next());
+            v2.add((ASN1Encodable) i.next());
         }
 
         v.add(new DERSequence(v2));
--- a/src/main/java/org/glite/voms/ac/ObjectDigestInfo.java
+++ b/src/main/java/org/glite/voms/ac/ObjectDigestInfo.java
@@ -28,19 +28,21 @@
 
 package org.glite.voms.ac;
 
+import java.io.IOException;
+
 import org.bouncycastle.asn1.ASN1EncodableVector;
 import org.bouncycastle.asn1.ASN1Sequence;
 import org.bouncycastle.asn1.DERBitString;
-import org.bouncycastle.asn1.DEREncodable;
+import org.bouncycastle.asn1.ASN1Encodable;
 import org.bouncycastle.asn1.DEREnumerated;
-import org.bouncycastle.asn1.DERObject;
+import org.bouncycastle.asn1.ASN1Primitive;
 import org.bouncycastle.asn1.DERObjectIdentifier;
 import org.bouncycastle.asn1.DERSequence;
 import org.bouncycastle.asn1.DERTaggedObject;
 import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
 
 
-public class ObjectDigestInfo implements DEREncodable {
+public class ObjectDigestInfo implements ASN1Encodable {
     DEREnumerated digestedObjectType;
     DERObjectIdentifier otherObjectTypeID;
     AlgorithmIdentifier digestAlgorithm;
@@ -58,7 +60,11 @@
 
         digestAlgorithm = AlgorithmIdentifier.getInstance(seq.getObjectAt(1 + offset));
 
-        objectDigest = new DERBitString(seq.getObjectAt(2 + offset));
+        try {
+            objectDigest = new DERBitString(seq.getObjectAt(2 + offset));
+        } catch (IOException e) {
+            throw new RuntimeException(e);
+        }
     }
 
     public DEREnumerated getDigestedObjectType() {
@@ -93,7 +99,7 @@
      *  }
      * </pre>
      */
-    public DERObject getDERObject() {
+    public ASN1Primitive toASN1Primitive() {
         ASN1EncodableVector v = new ASN1EncodableVector();
 
         v.add(digestedObjectType);
--- a/src/main/java/org/glite/voms/ac/V2Form.java
+++ b/src/main/java/org/glite/voms/ac/V2Form.java
@@ -33,10 +33,11 @@
 import org.bouncycastle.asn1.ASN1EncodableVector;
 import org.bouncycastle.asn1.ASN1Sequence;
 import org.bouncycastle.asn1.ASN1TaggedObject;
-import org.bouncycastle.asn1.DEREncodable;
-import org.bouncycastle.asn1.DERObject;
+import org.bouncycastle.asn1.ASN1Encodable;
+import org.bouncycastle.asn1.ASN1Primitive;
 import org.bouncycastle.asn1.DERSequence;
 import org.bouncycastle.asn1.DERTaggedObject;
+import org.bouncycastle.asn1.x509.GeneralName;
 import org.bouncycastle.asn1.x509.GeneralNames;
 import org.bouncycastle.asn1.x509.IssuerSerial;
 
@@ -44,7 +45,7 @@
 /**
  * @author mulmo
  */
-public class V2Form implements DEREncodable {
+public class V2Form implements ASN1Encodable {
     GeneralNames issuerName;
     IssuerSerial baseCertificateID;
     ObjectDigestInfo objectDigestInfo;
@@ -57,7 +58,7 @@
         int n = 0;
 
         if (seq.getObjectAt(0) instanceof ASN1Sequence) {
-            issuerName = new GeneralNames((ASN1Sequence) seq.getObjectAt(0));
+            issuerName = new GeneralNames(GeneralName.getInstance(seq.getObjectAt(0)));
             n++;
         }
 
@@ -66,7 +67,7 @@
 
             switch (tObj.getTagNo()) {
             case 0:
-                baseCertificateID = new IssuerSerial((ASN1Sequence) tObj.getObject());
+                baseCertificateID = IssuerSerial.getInstance((ASN1Sequence) tObj.getObject());
 
                 break;
 
@@ -106,12 +107,12 @@
      *  }
      * </pre>
      */
-    public DERObject getDERObject() {
+    public ASN1Primitive toASN1Primitive() {
         ASN1EncodableVector v = new ASN1EncodableVector();
 
         if (issuerName != null) {
             // IMPLICIT encoding of GeneralNames ... gosh, how I hate ASN.1 sometimes.
-            v.add(((ASN1Sequence) issuerName.getDERObject()).getObjectAt(0));
+            v.add(((ASN1Sequence) issuerName.toASN1Primitive()).getObjectAt(0));
         }
 
         if (baseCertificateID != null) {
--- a/src/main/java/org/glite/voms/contact/MyProxyCertInfo.java
+++ b/src/main/java/org/glite/voms/contact/MyProxyCertInfo.java
@@ -31,12 +31,12 @@
 import org.bouncycastle.asn1.ASN1EncodableVector;
 import org.bouncycastle.asn1.ASN1InputStream;
 import org.bouncycastle.asn1.ASN1Sequence;
-import org.bouncycastle.asn1.DEREncodable;
+import org.bouncycastle.asn1.ASN1Encodable;
 import org.bouncycastle.asn1.DERInteger;
-import org.bouncycastle.asn1.DERObject;
+import org.bouncycastle.asn1.ASN1Primitive;
 import org.bouncycastle.asn1.DERSequence;
 
-public class MyProxyCertInfo implements DEREncodable {
+public class MyProxyCertInfo implements ASN1Encodable {
 
     private int pathLen;
     private ProxyPolicy policy;
@@ -99,7 +99,7 @@
     }
 
     public MyProxyCertInfo(byte[] payload) {
-        DERObject derObj = null;
+        ASN1Primitive derObj = null;
         try {
             ByteArrayInputStream inStream = new ByteArrayInputStream(payload);
             ASN1InputStream derInputStream = new ASN1InputStream(inStream);
@@ -115,7 +115,7 @@
             throw new IllegalArgumentException("Unable to convert byte array");
     }
 
-    public DERObject getDERObject() {
+    public ASN1Primitive toASN1Primitive() {
         ASN1EncodableVector vec = new ASN1EncodableVector();
 
         switch(version) {
@@ -123,11 +123,11 @@
             if (this.pathLen != -1) {
                 vec.add(new DERInteger(this.pathLen));
             }
-            vec.add(this.policy.getDERObject());
+            vec.add(this.policy.toASN1Primitive());
             break;
 
         case VOMSProxyBuilder.GT4_PROXY:
-            vec.add(this.policy.getDERObject());
+            vec.add(this.policy.toASN1Primitive());
             if (this.pathLen != -1) {
                 vec.add(new DERInteger(this.pathLen));
             }
--- a/src/main/java/org/glite/voms/contact/ProxyPolicy.java
+++ b/src/main/java/org/glite/voms/contact/ProxyPolicy.java
@@ -24,16 +24,16 @@
  *********************************************************************/
 package org.glite.voms.contact;
 
-import org.bouncycastle.asn1.DEREncodable;
+import org.bouncycastle.asn1.ASN1Encodable;
 import org.bouncycastle.asn1.ASN1EncodableVector;
-import org.bouncycastle.asn1.DERObject;
+import org.bouncycastle.asn1.ASN1Primitive;
 import org.bouncycastle.asn1.ASN1Sequence;
 import org.bouncycastle.asn1.DERSequence;
 import org.bouncycastle.asn1.DERObjectIdentifier;
 import org.bouncycastle.asn1.DERTaggedObject;
 import org.bouncycastle.asn1.DEROctetString;
 
-public class ProxyPolicy implements DEREncodable {
+public class ProxyPolicy implements ASN1Encodable {
     private DERObjectIdentifier oid;
     private DEROctetString      policy;
 
@@ -61,7 +61,7 @@
         this.policy= null;
     }
 
-    public DERObject getDERObject() {
+    public ASN1Primitive toASN1Primitive() {
         ASN1EncodableVector vec = new ASN1EncodableVector();
 
         vec.add(oid);
@@ -74,7 +74,7 @@
     public ProxyPolicy(ASN1Sequence seq) {
         this.oid = (DERObjectIdentifier)seq.getObjectAt(0);
         if (seq.size() > 1) {
-            DEREncodable obj = seq.getObjectAt(1);
+            ASN1Encodable obj = seq.getObjectAt(1);
             if (obj instanceof DERTaggedObject) {
                 obj = ((DERTaggedObject)obj).getObject();
             }
--- a/src/main/java/org/glite/voms/contact/VOMSProxyBuilder.java
+++ b/src/main/java/org/glite/voms/contact/VOMSProxyBuilder.java
@@ -69,7 +69,7 @@
 import org.bouncycastle.asn1.ASN1EncodableVector;
 import org.bouncycastle.asn1.DERSequence;
 import org.bouncycastle.asn1.ASN1Sequence;
-import org.bouncycastle.asn1.DERObject;
+import org.bouncycastle.asn1.ASN1Primitive;
 import org.bouncycastle.asn1.DERSet;
 
 import org.bouncycastle.asn1.DERObjectIdentifier;
@@ -85,10 +85,10 @@
 
 class ExtensionData {
     String oid;
-    DERObject obj;
+    ASN1Primitive obj;
     boolean critical;
 
-    public static ExtensionData creator(String oid, boolean critical, DERObject obj) {
+    public static ExtensionData creator(String oid, boolean critical, ASN1Primitive obj) {
         ExtensionData ed = new ExtensionData();
         ed.obj = obj;
         ed.oid = oid;
@@ -97,7 +97,7 @@
         return ed;
     }
 
-    public static ExtensionData creator(String oid, DERObject obj) {
+    public static ExtensionData creator(String oid, ASN1Primitive obj) {
         ExtensionData ed = new ExtensionData();
         ed.obj = obj;
         ed.oid = oid;
@@ -110,7 +110,7 @@
         return oid;
     }
 
-    public DERObject getObj() {
+    public ASN1Primitive getObj() {
         return obj;
     }
 
@@ -225,7 +225,7 @@
         KeyUsage keyUsage = new KeyUsage( KeyUsage.digitalSignature
                 | KeyUsage.keyEncipherment | KeyUsage.dataEncipherment );
         extensions.put("2.5.29.15", ExtensionData.creator("2.5.29.15", true,
-                                                          keyUsage.getDERObject()));
+                                                          keyUsage.toASN1Primitive()));
 
         return myCreateCredential(
                   cred.getUserChain(),
@@ -375,11 +375,11 @@
                 if (gtVersion == GT3_PROXY)
                     extensions.put(PROXY_CERT_INFO_V3_OID,
                                    ExtensionData.creator(PROXY_CERT_INFO_V3_OID,
-                                                         new MyProxyCertInfo(policy, gtVersion).getDERObject()));
+                                                         new MyProxyCertInfo(policy, gtVersion).toASN1Primitive()));
                 else
                     extensions.put(PROXY_CERT_INFO_V4_OID,
                                    ExtensionData.creator(PROXY_CERT_INFO_V4_OID, true,
-                                                         new MyProxyCertInfo(policy, gtVersion).getDERObject()));
+                                                         new MyProxyCertInfo(policy, gtVersion).toASN1Primitive()));
             }
         }
 
@@ -398,11 +398,11 @@
         vec.add(X509Name.CN);
         vec.add(new DERPrintableString(cnValue));
             
-        Enumeration DNComponents = ((ASN1Sequence)issuerDN.getDERObject()).getObjects();
+        Enumeration DNComponents = ((ASN1Sequence)issuerDN.toASN1Primitive()).getObjects();
         ASN1EncodableVector subject = new ASN1EncodableVector();
 
         while (DNComponents.hasMoreElements())
-            subject.add(((DERObject)DNComponents.nextElement()));
+            subject.add(((ASN1Primitive)DNComponents.nextElement()));
         
         subject.add(new DERSet(new DERSequence(vec)));
 
